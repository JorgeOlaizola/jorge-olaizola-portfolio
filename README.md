# Jorge Olaizola Portfolio

## Try it

Link to deploy: https://jorgeolaizola.netlify.app/

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/JorgeOlaizola/jorge-olaizola-portfolio.git
```

Once the project it´s cloned, install dependencies

```bash
  npm install
```

Execute the following command to run the project in http://localhost:8000

```bash
  npm start
```