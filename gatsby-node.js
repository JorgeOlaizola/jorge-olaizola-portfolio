const path = require('path');
const fs = require('fs');

exports.createPages = ({ actions }) => {
  const { createPage } = actions;
  const data = JSON.parse(fs.readFileSync('./src/projects.json', { encoding: 'utf-8' }));

  const projectTemplate = path.resolve('src/templates/Project.tsx');
  data.projects.forEach((project) => {
    createPage({
      path: project.slug,
      component: projectTemplate,
      context: {project, tags: data.tags},
      });
  })
};
