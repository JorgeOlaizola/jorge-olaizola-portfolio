import React from 'react';
import './Presentation.css';

export default function Presentation() {
  return (
    <div className='flex justify-center items-center flex-col'>
      <img
        className='ProfilePicture'
        src='https://res.cloudinary.com/jorgeleandroolaizola/image/upload/v1627877406/MyImgs/Foto_CV_xdlg3j.jpg'
        alt='Jorge Olaizola'
      />
      <div className='InfoCont'>
        <p> Hi! Welcome to my portfolio.</p>
        <p>
          {' '}
          I'm a 22 years old full stack developer. I have studied in Henry´s bootcamp at the
          beginning of 2021.
        </p>
        <p> Im currently working at Applaudo studios in the trainee program as a React developer</p>
      </div>
    </div>
  );
}
