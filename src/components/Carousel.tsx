import React from 'react';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

function CarouselContainer({ images }) {
  return (
    <Carousel className='mt-4 mb-4'>
      {images.map((image, i) => (
        <img className='' src={image} alt='carousel' />
      ))}
    </Carousel>
  );
}

export default CarouselContainer;
