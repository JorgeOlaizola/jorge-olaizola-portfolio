import * as React from "react"
import Navbar from './Navbar';

import "./layout.css"

const Layout = ({ children }) => {

  return (
    <div className='bg-gradient-to-r min-h-screen from-gray-800 to-gray-600'>
      <Navbar />
        <main className='flex justify-center items-center'>{children}</main>
        <footer className='flex justify-center items-center bg-gray-100 h-20'>
          © {new Date().getFullYear()}, Jorge Leandro Olaizola
        </footer>
    </div>
  )
}

export default Layout
