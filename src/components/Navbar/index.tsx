import { Link } from 'gatsby';
import { StaticImage } from 'gatsby-plugin-image';
import React from 'react';

function index() {
  const clickFunction = () => {
    const mobileMenu = document.querySelector('.mobile-menu');
    mobileMenu.classList.toggle('hidden');
  };
  return (
    <nav className='bg-gray-100'>
      <div className='px-8 mx-auto border py-5'>
        <div className='flex justify-between'>
          <div className='flex items-center space-x-5'>
            <Link to='/'>
              <StaticImage
                src='../../images/logo.png'
                alt='logo'
                width={150}
                formats={['auto', 'webp', 'avif']}
                quality={95}
              />
            </Link>
          </div>
          <div className='hidden md:flex items-center space-x-5'>
            <Link to='/' className='py-2 px-3 text-gray-700 hover:text-gray-500'>
              About
            </Link>
            <Link to='/Projects' className='py-5 px-3 text-gray-700 hover:text-gray-500'>
              Projects
            </Link>
            <Link to='/Contact' className='py-5 px-3 text-gray-700 hover:text-gray-500'>
              Contact
            </Link>
          </div>
          <div onClick={clickFunction} style={{ cursor: 'pointer' }} className='md:hidden flex items-center mobile-menu-button'>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              className='h-6 w-6'
              fill='none'
              viewBox='0 0 24 24'
              stroke='currentColor'
            >
              <path
                stroke-linecap='round'
                stroke-linejoin='round'
                stroke-width='2'
                d='M4 6h16M4 12h16M4 18h16'
              />
            </svg>
          </div>
        </div>
        <div className='hidden md:hidden mobile-menu mt-3'>
          <Link className='block py-2 px-4 text-sm hover:bg-gray-200' to='/'>
            About
          </Link>
          <Link className='block py-2 px-4 text-sm hover:bg-gray-200' to='/Projects'>
            Projects
          </Link>
          <Link className='block py-2 px-4 text-sm hover:bg-gray-200' to='/Contact'>
            Contact
          </Link>
        </div>
      </div>
    </nav>
  );
}

export default index;
