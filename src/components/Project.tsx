import { Link } from 'gatsby';
import React from 'react';

function Project({ props, tagList }) {
  const { title, images, description, slug, releaseDate, url, repository, tags } = props;
  return (
    <>
      <section className='w-2/3 flex flex-col justify-center p-2'>
        <article className='group bg-gray-700 flex flex-wrap lg:flex-nowrap md:flex-inherit rounded-lg shadow-xl relative overflow-hidden flex-row bg-gradient-to-r from-gray-300 to-gray-200'>
          <Link to={`/${slug}`} className='contents'>
            <img
              className='p-5 relative w-full md:max-w-xs md:max-h-full max-h-48 object-fill'
              src={images[0]}
              alt='ProjectLogo'
            />
          </Link>
          <div className='relative flex flex-col p-4 w-2/3'>
            <h1 className='text-3x1 mb-2'>{title}</h1>
            <div className='small mb-2 text-small'>
              <time dateTime='2021-07-10 12:00:00' className=''>
                {releaseDate}
              </time>
            </div>
            <div className='w-0 group-hover:w-48 h-1 rounded bg-gray-900 duration-150'></div>
            <div className='text-justify text-lg'>{description}</div>
            <ul className='flex flex-row flex-wrap justify-center mt-8 md:justify-start text-gray-200'>
              {tagList.map((t) =>
                tags.includes(t.id) ? (
                  <div className='inline-block px-4 py-1 bg-gray-900 rounded mr-2 mb-1'>
                    {t.tag}
                  </div>
                ) : null
              )}
            </ul>
            <ul className='flex text-gray-200 flex-row flex-wrap justify-start w-full mt-8 md:justify-end'>
              <a
                href={url}
                className='inline-block px-4 py-1 bg-gray-900 hover:bg-gray-800 rounded mr-2 cursor-pointer mb-1'
              >
                Deploy
              </a>
              <a
                href={repository}
                className='inline-block px-4 py-1 bg-gray-900 hover:bg-gray-800 rounded mr-2 cursor-pointer mb-1'
              >
                Repository
              </a>
              <Link
                to={`/${slug}`}
                className='inline-block px-4 py-1 bg-gray-900 hover:bg-gray-800 rounded mr-2 cursor-pointer mb-1'
              >
                Detail
              </Link>
            </ul>
          </div>
        </article>
      </section>
    </>
  );
}

export default Project;
