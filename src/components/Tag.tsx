import React from 'react'

function Tag({ props }) {
    const { tag, id, image} = props
    return (
        <div className='w-16 h-48 flex justify-center items-center flex-col'>
            <h5>{tag}</h5>
            <img src={image} alt={tag} className='w-full' />
        </div>
    )
}

export default Tag
