import * as React from 'react';
import { StaticImage } from 'gatsby-plugin-image';
import { Link } from 'gatsby';

import Seo from '../components/seo';

const NotFoundPage = () => (
  <div className='flex justify-center items-center flex-col'>
    <Seo title='404: Not found' />
    <StaticImage
      src='../../images/logo.png'
      alt='logo'
      width={150}
      formats={['auto', 'webp', 'avif']}
      quality={95}
    />
    <h1 className='text-red-500'>404: Not Found</h1>
    <p>You just hit a route that doesn&#39;t exist...</p>
    <div className='flex justify-center items-center'>
      <Link to='/' className='bg-purple-800 p-4 text-white rounded-lg'>Return home</Link>
    </div>
  </div>
);

export default NotFoundPage;
