import React, { useState } from 'react';
import MainLayout from '../components/MainLayout';
import { FaGithub, FaGitlab, FaLinkedin } from 'react-icons/fa';

export default function Contact() {
  const [message, setMessage] = useState('');
  const onSubmit = (e) => {
    e.preventDefault();
    setMessage('Your message have been sent! I´ll answer you as soon as I can. Thank you for contacting me!')
  };

  return (
    <MainLayout>
      <div className='bg-gray-900 w-3/4 mt-10 p-10 flex flex-col justify-center items-center text-white rounded-lg border-solid border-white border-2 mb-10'>
        <div className='flex flex-col justify-evenly w-11/12 lg:flex-row'>
          <div className='flex justify-center flex-col items-center'>
            <h4 className='flex flex-row gap-2'>
              Contact me!
              <svg
                xmlns='http://www.w3.org/2000/svg'
                className='h-6 w-6'
                fill='none'
                viewBox='0 0 24 24'
                stroke='currentColor'
              >
                <path
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  strokeWidth={2}
                  d='M7 8h10M7 12h4m1 8l-4-4H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-3l-4 4z'
                />
              </svg>
            </h4>
            <ul>
              <li>
                Email: <span>jorgelolaizola@hotmail.com</span>
              </li>
              <li>
                Cellphone: <span>(+54) 11-6734-0113</span>
              </li>
            </ul>
          </div>
          <div className='flex justify-center flex-col items-center'>
            <h4 className='flex flex-row gap-2'>
              Follow me on social media{' '}
              <svg
                xmlns='http://www.w3.org/2000/svg'
                className='h-6 w-6'
                fill='none'
                viewBox='0 0 24 24'
                stroke='currentColor'
              >
                <path
                  stroke-linecap='round'
                  stroke-linejoin='round'
                  stroke-width='2'
                  d='M3 15a4 4 0 004 4h9a5 5 0 10-.1-9.999 5.002 5.002 0 10-9.78 2.096A4.001 4.001 0 003 15z'
                />
              </svg>
            </h4>
            <ul>
              <a
                className='flex flex-row gap-2 hover:text-purple-500'
                href='https://github.com/JorgeOlaizola'
              >
                GitHub <FaGithub />
              </a>
              <a
                className='flex flex-row gap-2 hover:text-purple-500'
                href='https://gitlab.com/JorgeOlaizola'
              >
                GitLab <FaGitlab />
              </a>
              <a
                className='flex flex-row gap-2 hover:text-purple-500'
                href='https://www.linkedin.com/in/jorge-olaizola/'
              >
                LinkedIn <FaLinkedin />
              </a>
            </ul>
          </div>
        </div>
        <span className='bg-white w-11/12 h-1 mt-10 mb-10'></span>
        {message && <div className='mb-5 text-purple-600 font-bold'>{message}</div>}
        <form
          onSubmit={onSubmit}
          className='flex flex-col justify-center items-center gap-4 w-11/12'
        >
          <input type='text' placeholder='Your name' className='rounded-md p-3 w-11/12 text-black md:w-2/3' required/>
          <input
            type='email'
            placeholder='Your email'
            className='rounded-md p-3 w-11/12 text-black md:w-2/3'
            required
          />
          <textarea placeholder='Your message' className='rounded-md p-3 w-11/12 text-black md:w-2/3' required/>
          <button className='p-3 w-20 bg-purple-800 hover:bg-purple-500 rounded-md border-solid border-white border-2'>
            Send
          </button>
        </form>
      </div>
    </MainLayout>
  );
}
