import React, { useState } from 'react';
import MainLayout from '../components/MainLayout';
import JSONData from '../projects.json';
import Project from '../components/Project';

export default function projects() {
  const [projectsToShow, filterProjects] = useState(JSONData.projects)
  const handleFilter = (e) => {
    if(parseInt(e.target.value) === 0) return filterProjects(JSONData.projects)
    filterProjects(JSONData.projects.filter((project) => project.tags.includes(parseInt(e.target.value))))
  }
  return (
    <MainLayout>
      <div className='w-full gap-4 mt-10 flex flex-col justify-center items-center mb-10'>
        <div className='w-2/3 flex flex-col items-center justify-center p-2'>
          <h1 className='text-white'>Filter by technology</h1>
          <select onChange={handleFilter} className='w-4/12 p-4 rounded-lg'>
            <option value={0}>None</option>
            {JSONData.tags.map(({ tag, id }) => <option value={id}>{tag}</option>)}
          </select>
        </div>
        {projectsToShow.length > 0 ? projectsToShow.map((project) => (
          <Project props={project} tagList={JSONData.tags} />
        )) : <div className='text-white'>No results found.</div>}
      </div>
    </MainLayout>
  );
}
