import * as React from 'react';
import MainLayout from '../components/MainLayout';
import Presentation from '../components/About/Presentation';
import './index.css';

const IndexPage = () => (
  <MainLayout>
    <div className='AboutContainer'>
      <div className='MyInfoContainer'>
        <Presentation />
      </div>
    </div>
  </MainLayout>
);

export default IndexPage;
