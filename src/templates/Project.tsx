import React from 'react';
import MainLayout from '../components/MainLayout';
import Tag from '../components/Tag';
import CarouselContainer from '../components/Carousel';
import { Link } from 'gatsby';


function Project(props) {
  const { pageContext } = props;
  const { project, tags } = pageContext;
  const { title, description, url, repository, images, releaseDate } = project;


  return (
    <MainLayout>
      <div className='flex mb-10 relative h-full justify-center w-3/4 items-center flex-col mt-10 bg-gradient-to-r from-gray-200 to-gray-100 p-10 rounded-lg'>
        <Link to='/Projects' className='self-start inline-block px-4 py-1 bg-gray-900 hover:bg-gray-800 rounded mr-2 cursor-pointer mb-1 text-white'>⬅ Back</Link>
        <h1>{title}</h1>
        <img src={images[0]} />
        <div>{description}</div>
        <div>Released on {releaseDate}</div>
        <div className='w-2/3'>
            <CarouselContainer images={images.slice(1)} />
        </div>
        <h3>Developed with</h3>
        <div className='flex flex-row gap-4 flex-wrap'>
          {tags.map(({ id, image, tag }) =>
            project.tags.includes(id) ? <Tag props={{ id, image, tag }} /> : null
          )}
        </div>
      <div className='flex justify-around w-3/4'>
        <a className='inline-block px-4 py-1 bg-gray-900 hover:bg-gray-800 rounded mr-2 cursor-pointer mb-1 text-white' href={url}>Deploy</a>
        <a className='inline-block px-4 py-1 bg-gray-900 hover:bg-gray-800 rounded mr-2 cursor-pointer mb-1 text-white' href={repository}>Repository</a>
      </div>
      </div>
    </MainLayout>
  );
}

export default Project;
